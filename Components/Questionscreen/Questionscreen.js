import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AppRegistry,
  TouchableOpacity,
  Button,
  ScrollView,
} from 'react-native';
import Animbutton from './Animbutton';
import Exitscreen from '../Exitscreen/Exitscreen';
let arrnew=[]
arrnew.length=10
const jsonData={"quiz" :{
  "quiz1": {
    "question 1":{

    "correctoption":"option4",
    "options":{
      "option1":"Pune",
      "option2":"Mumbai",
      "option3":"kolkata",
      "option4":"Delhi"
    },
    "question" : "Q. Capital of india is __"
    },

    "question3":{
      "correctoption":"option3",
      "options":{
        "option1":"Chandragupta",
        "option2":"Akbar",
        "option3":"Chatrapati Shivaji Maharaj",
        "option4":"Vasco-de-Gama"
      },
      "question":"Q. Founder of Indian Navy __"
    },
    "question4":{
      "correctoption":"option1",
      "options": {
        "option1":"APJ Abdul Kalam",
        "option2":"Sarojini Naidu",
        "option3":"Mahatma Gandhi",
        "option4":"Homibhabha"
      },
      "question":"Q. __ konwn as Missile man"

    },
    "question5":{
      "correctoption":"option3",
      "options": {
        "option1":"26",
        "option2":"20",
        "option3":"36",
        "option4":"33"
      },
      "question":"Q. Total District in Maharashtra __"
    },
    "question6":{
      "correctoption":"option2",
      "options": {
        "option1":"UttarPradesh",
        "option2":"Maharashtra",
        "option3":"Rajasthan",
        "option4":"Bihar"
      },
      "question":"Q. Marathi is mothertongue of which State"
    },

    "question7":{
      "correctoption":"option4",
      "options":
    {
      "option1":"Nanded",
      "option2":"Pune",
      "option3":"Latur",
      "option4":"Ahmadnagar"
    },
    "question":"Q. Largest District in Maharashtra __"
    },
    "question8":{
      "correctoption":"option1",
      "options":
    {
      "option1":"Up",
      "option2":"Maharashtra",
      "option3":"Haryana",
      "option4":"Karnatak"
    },
    "question":"Q. Largest state according population __"
    },
    "question9":{
      "correctoption":"option1",
      "options":
    {
      "option1":"2011",
      "option2":"2013",
      "option3":"2014",
      "option4":"2010"
    },
    "question":"Q. India win cricket worldcup in__"
    },
    "question10":{
      "correctoption":"option1",
      "options":
    {
      "option1":"Kashmir",
      "option2":"Maharashtra",
      "option3":"Haryana",
      "option4":"Karnatak"
    },
    "question":"Q. State whose Border with Pakistan __"
    },
    "question2":{
      "correctoption":"option4",
      "options":
    {
      "option1":"200km",
      "option2":"380km",
      "option3":"520km",
      "option4":"720km"
    },
    "question":"Q. Length of Kokan kinarpati__"
    },
    "question11":{
      "correctoption":"option3",
      "options":
    {
      "option1":"26-March-1655",
      "option2":"20-Feb-1626",
      "option3":"19-Feb-1630",
      "option4":"19-Feb-1626"
    },
    "question":"Q. Birth date of Chatrapati Shivaji Maharaj"
    },
    "question12":{
      "correctoption":"option2",
      "options":
    {
      "option1":"Hindi Mahasagar",
      "option2":"Pacific",
      "option3":"Dead sea",
      "option4":"Arbic sea"
    },
    "question":"Q. Largest Ocean in world"
    },
    "question13":{
      "correctoption":"option2",
      "options":
    {
      "option1":"Amazon",
      "option2":"Nile",
      "option3":"Bramhputra",
      "option4":"Ganga"
    },
    "question":"Q. Longest river in world"
    },
    "question14":{
      "correctoption":"option1",
      "options":
    {
      "option1":"Jawarlal Nehru",
      "option2":"Indira Gandhi",
      "option3":"Atal Bhihari Vajpayi",
      "option4":"Narendra Modi"
    },
    "question":"Q. 1st Prime Minister of India"
    },
    "question15":{
      "correctoption":"option4",
      "options":
    {
      "option1":"Marathas-Mughals",
      "option2":"Marathas-Adilshahi",
      "option3":"Marathas-Nijamshah",
      "option4":"Marahas-Afganies"
    },
    "question":"Q. Third Battel of Panipat is in betweeen"
    },
    "question16":{
      "correctoption":"option4",
      "options":
    {
      "option1":"Akbar",
      "option2":"Bahadur khan",
      "option3":"Ali-zafar",
      "option4":"Bahadur shah"
    },
    "question":"Q. King of Mughal after death of Aurengzeb "
    },
    "question17":{
      "correctoption":"option1",
      "options":
    {
      "option1":"Ganga",
      "option2":"Bramhputra",
      "option3":"Godavari",
      "option4":"Yamuna"
    },
    "question":"Q. longest River in India"
    },
    "question18":{
      "correctoption":"option4",
      "options":
    {
      "option1":"Pune",
      "option2":"Ratnagiri",
      "option3":"Sangli",
      "option4":"Satara"
    },
    "question":"Q. Panhala placed in which District"
    },
    "question19":{
      "correctoption":"option1",
      "options":
    {
      "option1":"2:3",
      "option2":"2:5",
      "option3":"3:2",
      "option4":"5:2"
    },
    "question":"Q. The ratio of width of our National flag to its length is"
    },
    "question20":{
      "correctoption":"option2",
      "options":
    {
      "option1":"Orissa",
      "option2":"Kerla",
      "option3":"Asaam",
      "option4":"Karnatak"
    },
    "question":"Q. Kathakali' is a folk dance prevalent in which state?"
    },
    "question21":{
      "correctoption":"option3",
      "options":
    {
      "option1":"Rabindranath Tagore",
      "option2":"chandra Banerjee",
      "option3":"Mohammad Iqbal",
      "option4":"Jaidev"
    },
    "question":"Q. Who composed the famous song 'Sare Jahan SeAchha'? "
    },
    "question22":{
      "correctoption":"option2",
      "options":
    {
      "option1":"North zone",
      "option2":"Southern zone",
      "option3":"North eastern zone",
      "option4":"South eastern zone"
    },
    "question":"Q. The only zone in the country that produces gold is also rich in iron is "
    },
    "question23":{
      "correctoption":"option4",
      "options":
    {
      "option1":"1:4",
      "option2":"2:3",
      "option3":"2:5",
      "option4":"2:4"
    },
    "question":"Q. The percentage of earth surface covered by India is"
    },
    "question24":{
      "correctoption":"option2",
      "options":
    {
      "option1":"Namchi, Sikkim",
      "option2":"Mawsynram, Meghalaya",
      "option3":"Chamba, Himachal Pradesh",
      "option4":"Kokan,Maharashtra"
    },
    "question":"Q. The India's highest annual rainfall is reported at"
    },
    "question25":{
      "correctoption":"option3",
      "options":
    {
      "option1":"Sikkim",
      "option2":"Rajastan",
      "option3":"Madhyapradesh",
      "option4":"Haryana"
    },
    "question":"Q. The state having a largest area of forest cover in India is"
    },
    "question26":{
      "correctoption":"option4",
      "options":
    {
      "option1":"Rawat Bhata",
      "option2":"Tarapore",
      "option3":"Narora",
      "option4":"Kalpakkam"
    },
    "question":"Q. which atomic power station in India is built completely indigenously?"
    },
    "question27":{
      "correctoption":"option1",
      "options":
    {
      "option1":"Aravalis",
      "option2":"Vindhyas",
      "option3":"Satpuras",
      "option4":"Nilgiri hills"
    },
    "question":"Q. The oldest mountains in India are"
    },
    "question28":{
      "correctoption":"option1",
      "options":
    {
      "option1":"70%",
      "option2":"80%",
      "option3":"52%",
      "option4":"75%"
    },
    "question":"Q. The percentage of India's total population employed in agriculture is nearly"
    },
    "question29":{
      "correctoption":"option2",
      "options":
    {
      "option1":"Mango",
      "option2":"Coconut",
      "option3":"Sugarcane",
      "option4":"Cotton"
    },
    "question":"Q. Which of the following crops is regarded as a plantation crop?"
    },
   }
}

}
a=[10]
function getRandomInt() {
var a= Math.floor((Math.random() * 29) + 0);
for(var i=0;i<10;i++)
{
  if(a[i]!==a)
  {
    return a
    a[i]=a
  }
}

}

export default class Questionscreen extends Component {
 constructor(props){
  super(props);
    this.qno=getRandomInt()
   // this.score=0
  //  number=0
  const jdata= jsonData.quiz.quiz1
  arrnew=Object.keys(jdata).map( function(k) { return jdata[k]});
  this.state ={
  question:arrnew[this.qno].question,
  options:arrnew[this.qno].options,
  correctoption:arrnew[this.qno].correctoption,
  countCheck:0,
 // number=0,
  score:0,
 outoff:1,
 buttonclicked:true
  }
}
next(){
{
  if(this.state.outoff<10)
  {
    const count1 = this.state.outoff + 1   
    this.setState({ outoff: count1 })
    this.qno=getRandomInt();
    
     this.setState({ countCheck: 0, question: arrnew[this.qno].question, options: arrnew[this.qno].options,
       correctoption : arrnew[this.qno].correctoption})
      }
      else{
    this.props.navigation.navigate("Exitscreen", {score:this.state.score ,outoff:this.state.outoff})
        
      }

        
   } 
}
// prev(){
//   if(this.qno>0){
//     this.qno--;
//     this.setState({countCheck:0, question: arrnew[this.qno].question, options:arrnew[this.qno].options,
//       correctoptions:arrnew[this.qno].correctoption})
//   }
// }

// _answer(status,ans){

//   if(status == true){
//       const count = this.state.countCheck + 1
//       this.setState({ countCheck: count })
//       if(ans == this.state.correctoption ){
//         this.score += 1
//       }
//     }else{
//       const count = this.state.countCheck - 1
//       this.setState({ countCheck: count })
//       if(this.state.countCheck < 1 || ans == this.state.correctoption){
//       this.score -= 1
//      }
//     }

// }


_answer(status,ans){

if(status=true)
  {
    this.setState({buttonclicked:false})
     const count = this.state.countCheck + 1   
      this.setState({ countCheck: count })
     
      if(ans == this.state.correctoption && this.state.countCheck<1 ){
        const score1=this.state.score + 1
        this.setState({score:score1})
       } 

    }

  }
  render() {
    const { navigate } = this.props.navigation 
    let _this = this
    const currentOptions = this.state.options
    const count3 =this.state.buttonclicked
    const options = Object.keys(currentOptions).map( function(k) {
      return (  <View key={k} style={{margin:10}}>
        <Animbutton countCheck={_this.state.countCheck} effect={'tada'}
      _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} buttonclicked={count3}/>

      </View>)
    });
return (

  <View style={styles.container1}>

  <View style={styles.container}>

      <Text style={styles.QuestionStyle} >{this.state.question} </Text>
      </View>
      

  <View>

      {options}

  </View>
      
      <View style={styles.container2}>
        {/* <Text style ={{paddingLeft:15}}> {this.state.score} </Text> */}
     
    
      
       <View style={styles.button1}> 
       <Button onPress={() => navigate("Exitscreen", {score:this.state.score ,outoff:this.state.outoff})}
       title='Finish'
       
       />

       </View>
       
       {/* <View  style={styles.button1}>
       <Button onPress={() =>this.prev() }
       title='Previous'
       />
       </View> */}

       <View style={styles.button2}>
       <Button onPress={() =>this.next() }
       title='Next'
       
       />
      

       </View>
  
         </View>
</View>

      );
    }
  }
  const styles = StyleSheet.create({
    container1: {
      flex:1,
//alignItems:'center',
  //    justifyContent:'center',
      backgroundColor:'#038387',
     // borderRadius: 4,
     // borderWidth: 2,
     //borderColor: 'white',
 },
container: {
   width:380,
   alignItems:'center',
      justifyContent:'center',
},
    QuestionStyle: {
      fontSize:20,
      fontFamily: 'Cochin',
      fontWeight:'bold',
      color:"white",
      paddingLeft:20,
      paddingRight:20,
      paddingTop:30,
      justifyContent:'center',
  
    },
   

   button1:{
     width:100,
     //height:200,
//flex:1
//flexDirection:'row',
   // padding:30,
   // justifyContent:'spa'
   
   
    },
    Message:
  {
    fontSize:40,
  },

container2:{
//flex:1,
  //width:200,
  //height:100,
  flexDirection:'row',
  justifyContent:'space-between',
  padding:40,
  paddingLeft:60,
  paddingRight:60
  //alignItems:'center'
  
},
button2:{
 // flex:1,
  width:100,
 // height:200,
  
}

  
});
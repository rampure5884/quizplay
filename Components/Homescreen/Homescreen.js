import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  Button
} from 'react-native';
import Questionscreen from '../Questionscreen/Questionscreen';

export default class Homescreen extends Component {

 render() {
  const { navigate } = this.props.navigation
  return (
<View style={styles.container1}>
<Image source={require('./image.png')} style={styles.imagestyle}/>


<Text style={styles.title}>Quiz Play </Text>
<Button

onPress={() => navigate("Questionscreen", {screen: "Screen Two"})}

  
  title="Start Test"
/>
</View>
 );
 }
}
const styles = StyleSheet.create({
  container1: {
    flex: 1,
    backgroundColor:'#038387',
    alignItems:'center',
    padding:60,
    justifyContent:'center',
    borderRadius: 4,
     borderWidth: 2,
    borderColor: 'white',
  },

  title: {
    color:'white',
    fontSize:30,
    fontWeight:'bold',
    alignItems:'center',
    justifyContent:'center',
    padding:55,
  },
  button:{
    alignItems: 'center',
    backgroundColor: '#2c3e50',
    padding:17
  },
  imagestyle:{
    flex:1,
    height:100,
    width:300,
    alignItems:'center',
    justifyContent:'center',
    padding:20
  }
});
  
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AppRegistry,
  TouchableOpacity,
  Button,
  ScrollView,
  Image,
  StatusBar,
  Alert
} from 'react-native';
import Questionscreen from '../Questionscreen/Questionscreen';
//import {Icon } from 'react-native-elements'


//import Icon from 'react-native-vector-icons/FontAwesome';
export default class Exitscreen extends Component{
  constructor(props){
    super(props)
    this.state = {
     // quizFinish : false,
      score:this.props.navigation.state.params.score ,
      outoff:this.props.navigation.state.params.outoff
    }
  }
     
  _scoreMessage(score){
    if(score <= 5){
      return (<View style={styles.innerContainer} >
      <View style={{ flexDirection: "row"}} >

      <Image source={require('./image3.png')}  style={styles.imagestyle} />

</View>
               
                <Text style={styles.score}> You need to work hard! </Text>
                <Text style={styles.score}> You scored {score} outoff {this.state.outoff}</Text>
              </View>)
    }else if(score > 5 && score < 10){
      return (<View style={styles.innerContainer} >
     <Image source={require('./image5.png')}  style={styles.imagestyle} />
 

                  <Text style={styles.score}> You are good </Text>
                  <Text style={styles.score}> Congrats you scored {score}  outoff {this.state.outoff} </Text>
                </View>)
    }else if(score >= 10){
      return (<View style={styles.innerContainer}>
      <Image source={require('./image6.png')}  style={styles.imagestyle} />

                
                  <Text style={styles.score}>!!Champion!! </Text>
                  <Text style={styles.score}>Congrats you scored {score} outoff {this.state.outoff} </Text>
                </View>)
    }
  }
 render(){
        return(
            <View style={styles.container}>
              { this._scoreMessage(this.state.score) }
              </View>

   );
    }

}
const scoreCircleSize = 300
const styles = StyleSheet.create({
  score: {
    color: "white",
    fontSize: 18,
    fontStyle: 'italic',
    //Color:'white',
   paddingLeft:15,
   paddingRight:15,
   justifyContent:'center',
   alignItems:'center'
  },
  circle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scoreCircleSize,
    height: scoreCircleSize,
    borderRadius: scoreCircleSize/2,
    backgroundColor: "green"
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#21759b',

  },
  toolbar:{
        backgroundColor:'#81c04d',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row'
    },
    toolbarButton:{
        width: 55,
        color:'#fff',
        textAlign:'center'
    },
    toolbarTitle:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'bold',
        flex:1
    },
    imagestyle:{
      
      height:200,
      width:300,
      alignItems:'center',
      justifyContent:'center',
      padding:20
    }
});